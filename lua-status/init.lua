---@diagnostic disable: lowercase-global

local eansi = require("eansi")
eansi.enable = os.getenv("NO_COLOR") == nil

---@version 5.1,5.2,5.3,5.4,JIT
---
---Prints a success status message to the standard error, with the `prefix` in green if the coloring has
---been enabled (i.e., the `NO_COLOR` environment variable is not set).
---
---```lua
--- status_success("Generated", "project")
--- --       Generated project
--- status_success("Found", string.format("Home at ${bold blue}%s", os.getenv("HOME")))
--- --       Found Home at /path/to/home
--- status_success("Generated", "project in the ${yellow}/path/to/project")
--- --       Generated project in the /path/to/project
--- status_success("Fetched", "value of ${red}$USER${reset}: " .. os.getenv("USER"))
--- --       Fetched value of $USER: username
---```
---
---@param prefix string
---@param suffix string
---
---@see string.format
function status_success(prefix, suffix)
    io.stderr:write(
        string.format("%5s%s %s\n", " ", eansi("${bold green}" .. (prefix or "")), eansi(suffix or ""))
    )
end

---@version 5.1,5.2,5.3,5.4,JIT
---
---Prints an informational status message to the standard error, with the `prefix` in cyan if the
---coloring has been enabled (i.e., the `NO_COLOR` environment variable is not set).
---
---```lua
--- status_info("Generating", "project")
--- --       Generating project
--- status_info("Searching", string.format("value of ${bold blue}$HOME"))
--- --       Searching value of $HOME
--- status_info("Creating", "project in the ${yellow}/path/to/project${reset} directory")
--- --       Creating project in the /path/to/project directory
---```
---
---@param prefix string
---@param suffix string
---
---@see string.format
function status_info(prefix, suffix)
    io.stderr:write(
        string.format("%5s%s %s\n", " ", eansi("${bold cyan}" .. (prefix or "")), eansi(suffix or ""))
    )
end

---@version 5.1,5.2,5.3,5.4,JIT
---
---Prints a task related status message to the standard error, with the `prefix` in magenta if the
---coloring has been enabled (i.e., the `NO_COLOR` environment variable is not set).
---
---```lua
--- status_task("Generating", "project")
--- --       Generating project
--- status_task("Searching", string.format("value of ${bold blue}$HOME"))
--- --       Searching value of $HOME
--- status_task("Creating", "project in the ${yellow}/path/to/project${reset} directory")
--- --       Creating project in the /path/to/project directory
---```
---
---@param prefix string
---@param suffix string
---
---@see string.format
function status_task(prefix, suffix)
    io.stderr:write(
        string.format("%5s%s %s\n", " ", eansi("${bold magenta}" .. (prefix or "")), eansi(suffix or ""))
    )
end

---@version 5.1,5.2,5.3,5.4,JIT
---
---Prints a warning status message to the standard error, with the `prefix` in yellow if the coloring
---has been enabled (i.e., the `NO_COLOR` environment variable is not set).
---
---```lua
--- status_warning("Did not found the project")
--- --warning: Did not found project
--- status_warning(string.format("Deleting the ${bold blue}$HOME${reset} directory"))
--- --warning: Deleting the $HOME directory
--- status_warning("Creating ${bold}project")
--- --warning: Creating project
---```
---
---@param message string
---
---@see string.format
function status_warning(message)
    io.stderr:write(
        string.format("%s %s\n", eansi("${bold yellow}" .. "warning:"), eansi(message or ""))
    )
end

---@version 5.1,5.2,5.3,5.4,JIT
---
---Prints an error status message to the standard error, with the `prefix` in red if the coloring has
---been enabled (i.e., the `NO_COLOR` environment variable is not set).
---
---```lua
--- status_error("Did not found the project")
--- --error: Did not found the project
--- status_error(string.format("${bold blue}$HOME${reset} directory not found"))
--- --error: $HOME directory not found
--- status_error("Failed to create ${bold}project")
--- --error: Failed to create project
---```
---
---@param message string
---
---@see string.format
function status_error(message)
    io.stderr:write(string.format("%s %s\n", eansi("${bold red}" .. "error:"), eansi(message or "")))
end
