<div align="center">

![LICENSE](https://img.shields.io/gitlab/license/53265450?style=for-the-badge&label=license&color=red)
![VERSION](https://img.shields.io/gitlab/v/tag/53265450?style=for-the-badge&label=version&color=green)

<div align="left">

> **`Lua status`** is a library meant to print status messages to the user, more specifically to the
> standard error, enhancing any command line application.

</div>

**[Report Bug or Request Features](https://gitlab.com/stote/lua-status/issues)**

</div>

## `Installation`

To initiate the installation process for the project, the following commands should be executed:

```bash
luarocks install lua-status
```

## `Usage`

At the root of the project just require the library like the following:

```lua
require("lua-status")
```

### `status_error`

Prints an error status message to the standard error, with the `prefix` in red if the coloring has
been enabled (i.e., the `NO_COLOR` environment variable is not set).

```lua
status_error("Did not found the project")
--error: Did not found the project
status_error(string.format("${bold blue}$HOME${reset} directory not found"))
--error: $HOME directory not found
status_error("Failed to create ${bold}project")
--error: Failed to create project
```

### `status_info`

Prints an informational status message to the standard error, with the `prefix` in cyan if the
coloring has been enabled (i.e., the `NO_COLOR` environment variable is not set).

```lua
status_info("Generating", "project")
--       Generating project
status_info("Searching", string.format("value of ${bold blue}$HOME"))
--       Searching value of $HOME
status_info("Creating", "project in the ${yellow}/path/to/project${reset} directory")
--       Creating project in the /path/to/project directory
```

### `status_success`

Prints a success status message to the standard error, with the `prefix` in green if the coloring has
been enabled (i.e., the `NO_COLOR` environment variable is not set).

```lua
status_success("Generated", "project")
--       Generated project
status_success("Found", string.format("Home at ${bold blue}%s", os.getenv("HOME")))
--       Found Home at /path/to/home
status_success("Generated", "project in the ${yellow}/path/to/project")
--       Generated project in the /path/to/project
status_success("Fetched", "value of ${red}$USER${reset}: " .. os.getenv("USER"))
--       Fetched value of $USER: username
```

### `status_task`

Prints a task related status message to the standard error, with the `prefix` in magenta if the
coloring has been enabled (i.e., the `NO_COLOR` environment variable is not set).

```lua
status_task("Generating", "project")
--       Generating project
status_task("Searching", string.format("value of ${bold blue}$HOME"))
--       Searching value of $HOME
status_task("Creating", "project in the ${yellow}/path/to/project${reset} directory")
--       Creating project in the /path/to/project directory
```

### `status_warning`

Prints a warning status message to the standard error, with the `prefix` in yellow if the coloring
has been enabled (i.e., the `NO_COLOR` environment variable is not set).

```lua
status_warning("Did not found the project")
--warning: Did not found project
status_warning(string.format("Deleting the ${bold blue}$HOME${reset} directory"))
--warning: Deleting the $HOME directory
status_warning("Creating ${bold}project")
--warning: Creating project
```

## `License`

This project is licensed under the [MIT](https://choosealicense.com/licenses/mit) license.
All files in the project carrying such notice may not be copied, modified, or distributed except
according to those [*terms*](LICENSE).

## `Changelog`

For a more comprehensive organized record of the modifications introduced throughout the project's
development, the [*CHANGELOG*](CHANGELOG.md) file can be referred to.
